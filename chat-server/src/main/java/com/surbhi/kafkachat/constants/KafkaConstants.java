package com.surbhi.kafkachat.constants;

public class KafkaConstants {
    public static final String KAFKA_TOPIC = "surbhi-chat-app";
    public static final String GROUP_ID = "kafka-sandbox";
    public static final String KAFKA_BROKER = "localhost:9092";
}
